from miplant import MiPlant
#https://github.com/kipe/miplant
#need to run as root

for plant in MiPlant.discover():
    print plant
    print 'temp: ',plant.temperature,' C'
    print 'light: ',plant.light, 'lux'
    print 'moisture: ', plant.moisture, '%'
    print 'conductivity: ', plant.conductivity, 'uS/cm'
    print 'battery: ', plant.battery, '%'